import re
import json
import jsonpickle
import sys
	
def readJSONFile(inputfile):
	with open(inputfile) as json_file:  
		data = json.load(json_file)
		return data

if __name__ == "__main__":
	if len(sys.argv) > 1 or len(sys.argv) < 3:
		data = readJSONFile(sys.argv[1])
		adData=data['adData']
		pageData=data['pageData']
		with open("data.json", mode='a') as feedsjson:
    			entry = {'id': adData['id'], 'time': adData['time'],'size':adData['size'],'screen': adData['screen'],'pageHeight': pageData[0]['height'],'pageId': pageData[0]['id'],'pageScreen':pageData[0]['screen'],'pageSize':pageData[0]['size'],'pageTime':pageData[0]['time'],'pageWidth':pageData[0]['width']}
			#entry = {'id': adData['id']/pageData['id'], 'time': adData['time']/pageData['time'],'size':adData['size']/pageData['size'],'screen': adData['screen']/pageData['screen']}
    			json.dump(entry, feedsjson)
			feedsjson.write('\n')
			feedsjson.close()
	else:
		print "Invalid args"
		sys.exit(2)

