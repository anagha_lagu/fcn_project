import re
import json
import jsonpickle
import sys
import statistics
import csv
	
def readJSONFile(inputfile):
	
	lines = open(inputfile).read().splitlines()
	print lines
	data =[]
	for line in lines:
		jfile = json.loads(line)
		data.append(jfile)
	print data            
	return data

if __name__ == "__main__":
	if len(sys.argv) > 2 or len(sys.argv) < 3:
		data = readJSONFile(sys.argv[1])
		adcount =[]
		adtime =[]
		adsize =[]
		adscreen =[]
		pageHeight =[]
		pageId =[]
		pageScreen =[]
		pageSize =[]
		pageTime =[]
		pageWidth =[]
		for adData in data:
			print adData
			adcount.append(adData['id'])
			adtime.append(adData['time'])
			adsize.append(adData['size'])
			adscreen.append(adData['screen'])
			pageHeight.append(adData['pageHeight'])
			pageId.append(adData['pageId'])
			pageScreen.append(adData['pageScreen'])
			pageSize.append(adData['pageSize'])
			pageTime.append(adData['pageTime'])
			pageWidth.append(adData['pageWidth'])
		#print adcount/6,totaltime/6.0,totalsize/6.0,totalscreen/6.0,pageHeight/6.0,pageId/6,pageScreen/6.0,pageSize/6.0,pageTime/6.0,pageWidth/6.0	
		#with open("out.json", mode='a') as feedsjson:
    			#entry = {'name':sys.argv[2],'id': adcount/6, 'time': totaltime/6.0,'size':totalsize/6.0,'screen':totalscreen/6.0,'pageHeight':pageHeight/6.0,'pageId':pageId/6,'pageScreen':pageScreen/6.0,'pageSize':pageSize/6.0,'pageTime':pageTime/6.0,'pageWidth':pageWidth/6.0}
    			#json.dump(entry, feedsjson,indent=4, sort_keys=True)
			#feedsjson.write('\n')
		print len(adcount)
		print len(adtime)
		print len(adscreen)
		print len(pageScreen)
		print statistics.median(adcount)
		with open('out.csv', 'a') as csvfile:
    			fieldnames = ['name','id', 'time','size','screen']
    			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writerow({'name': sys.argv[2],'id': statistics.median(adcount)/ statistics.median(pageId) , 'time':  statistics.median(adtime)/ statistics.median(pageTime),'size': statistics.median(adsize)/ statistics.median(pageSize),'screen': statistics.median(adscreen)/ statistics.median(pageScreen)})
			#writer.writerow({'name': sys.argv[2],'id': statistics.median(adcount), 'time':  statistics.median(adtime),'size': statistics.median(adsize),'screen': statistics.median(adscreen)})
			#writer.writerow({'name': sys.argv[2],'id': statistics.median(pageId) , 'time': statistics.median(pageTime),'size':statistics.median(pageSize),'screen': statistics.median(pageScreen)})

    	else:
		print "Invalid args"
		sys.exit(2)

