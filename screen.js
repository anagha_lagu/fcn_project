var system = require('system');
var fs = require('fs');
var args = system.args;
var page = require('webpage').create();
var url = "";
if (args.length === 1) {
  console.log('Try to pass some arguments when invoking this script!');
  phantom.exit()
} 
else {
  args.forEach(function(arg, i) {
    url = arg;
  });
}

page.open(url, function() {

    var ua = page.evaluate(function() {
      var body = document.body,html = document.documentElement;
				var height = Math.max( body.scrollHeight, body.offsetHeight, 
                       		html.clientHeight, html.scrollHeight, html.offsetHeight );

				var width = Math.max(document.documentElement["clientWidth"],document.body["scrollWidth"],
    					             document.documentElement["scrollWidth"],document.body["offsetWidth"],
    						         document.documentElement["offsetWidth"]);
		return [height,width];
    });

	var ub = page.evaluate(function() {
		var list = [];
    var a=document.getElementsByTagName('iframe');
    for(var i=0;i<a.length;i++){
        list[i] = [a[i].clientWidth,a[i].clientHeight];
    }
    return list;
    });
    
    console.log(ua[0]+"   "+ua[1]);
    console.log(ub);
    var sum = 0;
    for(var i=0;i<ub.length;i++){
    	console.log(ub[i][0]+"  "+ub[i][1]);
    	sum += ub[i][0]*ub[i][1];
    }
    console.log(sum);


	filedata = fs.read('ads.json');
	var resultObject = JSON.parse(filedata);
	resultObject.adData.screen = sum;
	resultObject.pageData[0].height = ua[0];
	resultObject.pageData[0].width = ua[1];
	resultObject.pageData[0].screen = (ua[0]*ua[1]);
	var json = JSON.stringify(resultObject,null,2);
    fs.write('ads.json', json, 'w');
    console.log("File written successfully !!!");

    phantom.exit()
  
});
