#!/bin/bash
echo "name,id,time,size,screen" > out.csv
while IFS='' read -r line || [[ -n "$line" ]]; do
	 if [ "$line" == "" ]; then
		echo $line
		echo "blank" 
		exit
	fi
	echo "Text read from file: $line"
	A="$(cut -d'.' -f2 <<<"$line")"
	B=$(awk -F. '{print $2}' <<< "$line")
	i=0
	rm -rf data.json
	for i in {0..5}; do
		#create resources.json with size and time for every URL 
		phantomjs confess.js $line performance 
		# parser will refer to an adserver list and seggregate ad urls from non ad urls and create ads,json with the required details like size, time adcount, urls
		python parser.py resource.json
		rm -rf resource.json
		# this code will add the screen dimension parameters to ads.json
		phantomjs screen.js $line
		
		python analysis.py ads.json
		rm -rf ads.json
	done
	python stats.py data.json $B
	rm -rf data.json
	sleep 5
	
done < "$1"
