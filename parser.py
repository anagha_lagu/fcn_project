import re
import json
import jsonpickle
import sys
	
def parseAdserverList():
	text_file = open("adserverlist.txt", "r")
	lines = text_file.read().split('\n')
	text_file.close()
	del lines[-1]
	return lines

def readJSONFile(inputfile):
	with open(inputfile) as json_file:  
		data = json.load(json_file)
		return data

def pageData(inputfile):
	with open(inputfile) as json_file:  
		data = json.load(json_file)
		return data

def identifyAds(data,lines,pageData):
	count =0
	totaltime =0
	totalspace=0
	adList =[]
	for t in data['table']:
		for adserver in lines:
			if adserver in t['url']:
				adList.append({'line':count+1,'time':str(t['time']),'size':str(t['size']),'url':t['url']})
				count= count+1
	for a in adList:
		if a['time'].isdigit() and a['size'].isdigit():
			totaltime= totaltime + int(a['time'])
			totalspace = totalspace + int(a['size'])
	print "Ads count: ",count
	data = {'ads':adList, 'adData':{'id':count,'time':totaltime,'size':totalspace, 'screen':0}, 'pageData':pageData["page"]}
	with open("ads.json", 'w') as out_file:
		json.dump(data,out_file,indent=4, sort_keys=True)
		

if __name__ == "__main__":
	if len(sys.argv) > 1 or len(sys.argv) < 3:
		data = readJSONFile(sys.argv[1])
		lines = parseAdserverList()
		pageData = pageData(sys.argv[1])
		identifyAds(data,lines,pageData)
	else:
		print "Invalid args"
		sys.exit(2)

